#!/usr/bin/env python3

import json
from random import random, gauss


def run():
    """Generate JSON data"""

    data = []
    for i in range(0, 65536):
        temp = {}
        temp["key"] = i
        temp["value"] = 0
        if i <= 1024:
            if random() < 0.40:
                temp["value"] = int(abs(gauss(50, 40)))
        else:
            if random() < 0.15:
                temp["value"] = int(abs(gauss(10, 9)))
        data.append(temp)

    with open("fake_port_data.json", "w") as port_data:
        json.dump(data, port_data)


if __name__ == "__main__":
    run()
