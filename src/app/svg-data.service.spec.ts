import { TestBed } from '@angular/core/testing';

import { SvgDataService } from './svg-data.service';

describe('SvgDataService', () => {
  let service: SvgDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SvgDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
