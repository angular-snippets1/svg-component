import { analyzeFileForInjectables } from '@angular/compiler';
import {
  AfterViewInit,
  Component,
  HostListener,
  ViewChild,
} from '@angular/core';
import { Box } from '@svgdotjs/svg.js';
import { SvgDataService } from './svg-data.service';
import { SvgComponent } from './svg/svg.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements AfterViewInit {
  title = 'draw-components';

  // this only works if openning the app in landscape mode
  gridW: number = null;
  gridH: number = null;
  sliceX: number = 256;
  sliceY: number = 256;
  offX: number = 10;
  offY: number = 10;
  lineWidth: number = null;
  box: Box = null;

  mainColour: string = '#0DA';
  details: any = null;

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    if (window.innerHeight >= window.innerWidth) {
      this.gridH = Math.round(window.innerWidth * 0.9);
    } else {
      this.gridW = Math.round(window.innerHeight * 0.9);
    }
    this.lineWidth = this.gridW / this.sliceX / 20;
  }

  private subscription = this.dataService.stations$.subscribe((x) =>
    this.dummyGridFilling(x)
  );

  constructor(private dataService: SvgDataService) {
    this.onResize();
  }

  @ViewChild(SvgComponent)
  private gridComponent: SvgComponent;

  ngAfterViewInit(): void {
    // this.dummyGridFilling();
  }

  _valueToCoordinate(value: number): { x: number; y: number } {
    return { x: Math.floor(+value / this.sliceY), y: +value % this.sliceX };
  }

  resetZoom(e: Event): void {
    console.log('Reset zoom in main component');
    this.gridComponent.resetZoom();
  }

  searchGrid(searchValue: string) {
    console.log(searchValue);

    let { x, y } = this._valueToCoordinate(+searchValue);

    this.resetZoom(null);
    if (this.gridComponent.hasGridPos(x, y)) {
      this.gridComponent.highlightGridPos(x, y);
      let gridPos = this.gridComponent.getGridPos(x, y);
      this.details = {
        col: x,
        line: y,
        port_number: searchValue,
        value: +gridPos.node.getAttribute('data-value'),
      };
      console.log(this.details);
    } else {
      this.gridComponent.highlightGridPos(x, y, '#E33');
    }
  }

  /** Dummy CSV exporter
   *  Download CSV data from every element of
   *  the grid currently visible in the viewbox.
   */
  exportVisible(): void {
    const delimiter = ',';
    // Should not be manually defined
    let header = ['key', 'value'].join(delimiter) + '\n';
    let csv = header;

    // Gather data from current viewbox
    let arrayData = this.gridComponent.exportVisible();
    arrayData.forEach((obj) => {
      let row = [];
      for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
          row.push(obj[key]);
        }
      }
      csv += row.join(delimiter) + '\n';
    });

    let csvData = new Blob([csv], { type: 'text/csv' });
    let csvUrl = URL.createObjectURL(csvData);

    let hiddenElement = document.createElement('a');
    hiddenElement.href = csvUrl;
    hiddenElement.target = '_blank';
    hiddenElement.download = 'exportGrid.csv';
    hiddenElement.click();
    hiddenElement.remove();
  }

  viewboxUpdate(box: Box): void {
    this.box = box;
  }

  dummyGridFilling(x: any) {
    x.forEach((e: any) => {
      if (e['value'] != 0) {
        let { x, y } = this._valueToCoordinate(+e['key']);

        if (e['value'] < 5) {
          this.gridComponent.fillGridPos(x, y, e['value'], '#9bf398');
        } else if (e['value'] < 10) {
          this.gridComponent.fillGridPos(x, y, e['value'], '#cef79f');
        } else if (e['value'] < 15) {
          this.gridComponent.fillGridPos(x, y, e['value'], '#effca1');
        } else if (e['value'] < 20) {
          this.gridComponent.fillGridPos(x, y, e['value'], '#fcda98');
        } else if (e['value'] < 25) {
          this.gridComponent.fillGridPos(x, y, e['value'], '#f6a395');
        } else if (e['value'] < 30) {
          this.gridComponent.fillGridPos(x, y, e['value'], '#ee698f');
        } else if (e['value'] < 45) {
          this.gridComponent.fillGridPos(x, y, e['value'], '#a82349');
        } else {
          this.gridComponent.fillGridPos(x, y, e['value'], '#3a0000');
        }
      }
    });
  }

  rectangleClicked(e: SVGRectElement) {
    this.gridComponent.addText(
      +e.getAttribute('data-line'),
      +e.getAttribute('data-col')
    );
  }

  rectangleEntered(e: SVGRectElement) {
    let col = +e.getAttribute('data-col');
    let line = +e.getAttribute('data-line');
    this.details = {
      col: col,
      line: line,
      port_number: line * this.sliceX + col,
      value: +e.getAttribute('data-value'),
    };
    this.gridComponent.highlightGridPos(line, col);
  }

  rectangleExited(e: SVGRectElement) {
    this.details = null;
    this.gridComponent.removeHighlight();
  }
}
