import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Box } from '@svgdotjs/svg.js';

@Component({
  selector: 'app-svg-details',
  templateUrl: './svg-details.component.html',
  styleUrls: ['./svg-details.component.scss'],
})
export class SvgDetailsComponent implements OnInit {
  @Input() details: any;
  @Input() box: Box;
  @Output() resetZoomClicked = new EventEmitter<Event>();
  @Output() exportClicked = new EventEmitter<Event>();
  @Output() searchClicked = new EventEmitter<string>();

  searchValue: string = '';

  constructor() {}

  ngOnInit(): void {}

  onClickResetZoom(e: Event): void {
    this.resetZoomClicked.emit(e);
  }

  onClickExport(e: Event): void {
    this.exportClicked.emit(e);
  }

  onClickFindElement(findValue: string): void {
    this.searchClicked.emit(findValue);
  }
}
