import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
} from '@angular/core';
import { Box } from '@svgdotjs/svg.js';
import { G, Rect, Svg, SVG, Text } from '@svgdotjs/svg.js';
import '@svgdotjs/svg.panzoom.js';

@Component({
  selector: 'app-svg',
  templateUrl: './svg.component.html',
  styleUrls: ['./svg.component.scss'],
})
export class SvgComponent implements OnChanges {
  @Input() height: number;
  @Input() width: number;
  @Input() xOffset: number = 10;
  @Input() yOffset: number = 10;
  @Input() xSlices: number = 4;
  @Input() ySlices: number = 4;
  @Input() lineWidth: number = 0.5;

  @Output() gridRectangleClicked = new EventEmitter<SVGRectElement>();
  @Output() gridRectangleEntered = new EventEmitter<SVGRectElement>();
  @Output() gridRectangleExited = new EventEmitter<SVGRectElement>();
  @Output() viewboxChanged = new EventEmitter<Box>();

  x_step: number = 0;
  y_step: number = 0;

  drawPanel: Svg = SVG();
  gr_table: G = new G();
  grid: Map<number, Map<number, G>> = new Map();
  legends: Array<Text> = new Array<Text>();

  private highlight: G = null;

  constructor() {}

  ngOnChanges(): void {
    this.x_step = (this.width - 2 * this.xOffset) / this.xSlices;
    this.y_step = (this.height - 2 * this.yOffset) / this.ySlices;

    this.drawPanel
      .viewbox(`0 0 ${this.width} ${this.height}`)
      .panZoom({ zoomMin: 1, zoomMax: 30, zoomFactor: 1 })
      .addTo('#svg-panel');

    this.drawPanel.on('zoom', () => {
      this.zoomPanEvent();
    });

    this.drawPanel.on('panning', () => {
      this.zoomPanEvent();
    });

    this.draw();
  }

  zoomPanEvent() {
    if (this.drawPanel.zoom() <= 16) {
      this.removeAllText();
      return;
    }

    this.viewboxChanged.emit(this.drawPanel.viewbox());
    let { x, y, width, height } = this.drawPanel.viewbox();
    let firstCol = Math.floor((x - this.xOffset) / this.x_step);
    let lastCol = Math.ceil((x + width - this.xOffset) / this.x_step);
    let firstLine = Math.floor((y - this.yOffset) / this.y_step);
    let lastLine = Math.ceil((y + height - this.yOffset) / this.y_step);

    if (firstCol < 0) {
      firstCol = 0;
    }

    if (lastCol >= this.xSlices) {
      lastCol = this.xSlices - 1;
    }

    if (firstLine < 0) {
      firstLine = 0;
    }

    if (lastLine >= this.ySlices) {
      lastLine = this.ySlices - 1;
    }

    for (let column = firstCol; column <= lastCol; column++) {
      for (let line = firstLine; line <= lastLine; line++) {
        if (this.hasGridPos(line, column)) {
          this.addText(line, column);
        }
      }
    }
  }

  /** Export raw data from every element of the grid currently
   * inside the drawPanel Viewbox window (probably not entirely
   * accurate on the borders, but it's just a dummy function to
   * return various data from the grid and test the CSV exporter)
   *
   * @returns Array<{key: string, value: string}>
   */
  exportVisible(): Array<{ key: string; value: string }> {
    let ret = new Array<{ key: string; value: string }>();

    let { x, y, width, height } = this.drawPanel.viewbox();
    let firstCol = Math.ceil((x - this.xOffset) / this.x_step);
    let lastCol = Math.floor((x + width - this.xOffset) / this.x_step);
    let firstLine = Math.ceil((y - this.yOffset) / this.y_step);
    let lastLine = Math.floor((y + height - this.yOffset) / this.y_step);

    if (firstCol < 0) {
      firstCol = 0;
    }

    if (lastCol >= this.xSlices) {
      lastCol = this.xSlices - 1;
    }

    if (firstLine < 0) {
      firstLine = 0;
    }

    if (lastLine >= this.ySlices) {
      lastLine = this.ySlices - 1;
    }

    for (let column = firstCol; column <= lastCol; column++) {
      for (let line = firstLine; line <= lastLine; line++) {
        if (this.hasGridPos(line, column)) {
          ret.push({
            key: this.getGridPos(line, column).get(0).data('key'),
            value: this.getGridPos(line, column).get(0).data('value'),
          });
        }
      }
    }

    return ret;
  }

  /**
   * Add text over rectangles inside the grid
   * (based on data attributes of each rectangle).
   *
   * @param line
   * @param column
   * @returns void
   */
  addText(line, column): void {
    let rect = this.getGridPos(line, column).get(0);

    if (this.getGridPos(line, column).get(1)) {
      return;
    }

    let text = new Text()
      .font({ size: 1, anchor: 'middle', fill: '#445' })
      .text((add) => {
        add.tspan(rect.data('key')).newLine();
        add.tspan(rect.data('value')).dx(-2).dy(1);
      })
      .center(
        column * this.x_step + this.xOffset + this.x_step / 2,
        line * this.y_step + this.yOffset + this.y_step / 2
      );

    text.addTo(this.getGridPos(line, column));
    this.legends.push(text);
  }

  removeAllText() {
    this.legends.forEach((elem) => elem.remove());
  }

  draw(): void {
    this.gr_table.remove();
    this.gr_table = new G();

    this.gr_table
      .polygon([
        [this.xOffset, this.yOffset],
        [this.width - this.xOffset, this.yOffset],
        [this.width - this.xOffset, this.height - this.yOffset],
        [this.xOffset, this.height - this.yOffset],
        [this.xOffset, this.yOffset],
      ])
      .fill('none')
      .stroke({ color: '#AAB', width: this.lineWidth });

    for (
      let y_idx: number = this.y_step;
      y_idx < this.height - 2 * this.yOffset;
      y_idx += this.y_step
    ) {
      this.gr_table
        .line(
          this.xOffset,
          this.yOffset + y_idx,
          -this.xOffset + this.width,
          this.yOffset + y_idx
        )
        .stroke({ color: '#AAB', width: this.lineWidth });
    }

    for (
      let x_idx: number = this.x_step;
      x_idx < this.width - 2 * this.xOffset;
      x_idx += this.x_step
    ) {
      this.gr_table
        .line(
          this.xOffset + x_idx,
          this.yOffset,
          this.xOffset + x_idx,
          -this.yOffset + this.height
        )
        .stroke({ color: '#AAB', width: this.lineWidth });
    }

    this.gr_table.addTo(this.drawPanel);
  }

  resetZoom(): void {
    this.drawPanel.animate().viewbox(0, 0, this.width, this.height);
    this.viewboxChanged.emit(this.drawPanel.viewbox());
  }

  private _gridRectClicked = (event: MouseEvent) => {
    let rectangle = event.target as SVGRectElement;
    // console.log("Clicked line " + rectangle.getAttribute("data-line")
    //  + " and col " + rectangle.getAttribute("data-col"));
    this.gridRectangleClicked.emit(rectangle);
  };

  private _gridRectEntered = (event: MouseEvent) => {
    let rectangle = event.target as SVGRectElement;
    // console.log("Entered line " + rectangle.getAttribute("data-line")
    //  + " and col " + rectangle.getAttribute("data-col"));
    this.gridRectangleEntered.emit(rectangle);
  };

  private _gridRectExited = (event: MouseEvent) => {
    let rectangle = event.target as SVGRectElement;
    // console.log("Exited line " + rectangle.getAttribute("data-line")
    //  + " and col " + rectangle.getAttribute("data-col"));
    this.gridRectangleExited.emit(rectangle);
  };

  _checkCoordinates(line: number, col: number): boolean {
    if (line >= this.ySlices || col >= this.xSlices) {
      console.log(
        `Given grid coordinates (${line}, ${col}) are out-of-bound. Skipping.`
      );
      return false;
    }
    return true;
  }

  getGridPos(line: number, col: number): G {
    if (this.hasGridPos(line, col)) {
      // console.log(this.grid.get(line).get(col));
      return this.grid.get(line).get(col);
    }

    throw 'Not found';
  }

  hasGridPos(line: number, col: number): boolean {
    if (!this._checkCoordinates(line, col)) {
      throw 'Out of boundary';
    }

    if (this.grid.has(line) && this.grid.get(line).has(col)) {
      return true;
    }

    return false;
  }

  fillGridPos(
    line: number,
    col: number,
    data: any = null,
    colour: string = '#000'
  ): void {
    if (!this._checkCoordinates(line, col)) {
      return;
    }

    // This grid coordinate was already drawn at least once.
    if (this.grid.has(line) && this.grid.get(line).has(col)) {
      // console.log('Changing color for existing grid rectangle.');
      this.grid.get(line).get(col).fill(colour);
      return;
    } else if (!this.grid.has(line)) {
      this.grid.set(line, new Map());
    }

    // console.log('New grid rectangle.');
    let gridGroup = new G();

    let gridRect = new Rect()
      .width(this.x_step - this.lineWidth)
      .height(this.y_step - this.lineWidth)
      .move(
        this.xOffset + col * this.x_step + this.lineWidth / 2,
        this.yOffset + line * this.y_step + this.lineWidth / 2
      )
      .fill(colour);

    gridRect.data('line', line);
    gridRect.data('col', col);
    gridRect.data('key', line * this.xSlices + col);
    gridRect.data('value', data);
    gridRect.data('colour', colour);
    gridRect.addTo(gridGroup);
    gridRect.on('click', this._gridRectClicked);
    gridRect.on('mouseenter', this._gridRectEntered);
    gridRect.on('mouseleave', this._gridRectExited);

    gridGroup.addTo(this.drawPanel);
    this.grid.get(line).set(col, gridGroup);
  }

  highlightGridPos(line: number, col: number, colour: string = '#2E8'): void {
    let xCoord = this.xOffset + this.x_step * col + this.x_step / 2;
    let yCoord = this.yOffset + this.y_step * line + this.y_step / 2;

    if (this.highlight) {
      this.highlight.remove();
    }
    this.highlight = new G();

    // top to pos
    this.highlight
      .line(xCoord, 0, xCoord, yCoord - this.y_step - 5)
      .stroke({
        color: colour,
        width: this.x_step - this.lineWidth,
        linecap: 'round',
      })
      .opacity(0)
      .delay(200)
      .animate()
      .attr({ opacity: 0.4 })
      .dmove(0, +5);
    // left to pos
    this.highlight
      .line(0, yCoord, xCoord - this.x_step - 5, yCoord)
      .stroke({
        color: colour,
        width: this.y_step - this.lineWidth,
        linecap: 'round',
      })
      .opacity(0)
      .delay(200)
      .animate()
      .attr({ opacity: 0.4 })
      .dmove(+5, 0);
    // bottom to pos
    this.highlight
      .line(
        xCoord,
        this.height + this.xOffset * 2,
        xCoord,
        yCoord + this.y_step + 5
      )
      .stroke({
        color: colour,
        width: this.x_step - this.lineWidth,
        linecap: 'round',
      })
      .opacity(0)
      .delay(200)
      .animate()
      .attr({ opacity: 0.4 })
      .dmove(0, -5);
    // right to pos
    this.highlight
      .line(
        this.width + this.yOffset * 2,
        yCoord,
        xCoord + this.x_step + 5,
        yCoord
      )
      .stroke({
        color: colour,
        width: this.y_step - this.lineWidth,
        linecap: 'round',
      })
      .opacity(0)
      .delay(200)
      .animate()
      .attr({ opacity: 0.4 })
      .dmove(-5, 0);

    this.highlight.addTo(this.drawPanel);
  }

  removeHighlight() {
    if (this.highlight) {
      // this.highlight.delay(50).animate().attr({"opacity": 0});
      this.highlight.remove();
    }
  }

  clearGridPos(line: number, col: number): void {
    if (!this._checkCoordinates(line, col)) {
      return;
    }

    if (this.grid.has(line) && this.grid.get(line).has(col)) {
      console.log('Clearing existing grid rectangle.');
      this.grid.get(line).get(col).remove();
      this.grid.get(line).delete(col);
    }
  }
}
