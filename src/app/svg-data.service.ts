import { Injectable } from '@angular/core';
import {
  tap,
  map,
  catchError,
  shareReplay,
  retryWhen,
  delay,
  take,
} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { EMPTY } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SvgDataService {
  private jsonDataURI: string = './assets/fake_port_data.json';

  constructor(private httpClient: HttpClient) {}

  stations$ = this.httpClient.get(this.jsonDataURI).pipe(
    retryWhen((errors) =>
      errors.pipe(
        delay(3000),
        take(2),
        tap(() => console.log('Error while fetching data, even after retries'))
      )
    ),
    shareReplay(1),
    tap((ports) => {
      console.log(
        'New item emitted by httpClient (and re-emitted to changeLogSubject)'
      );
      // this.changeLogSubject.next(stations);
    }),
    catchError(this.handleError)
  );

  handleError(err) {
    console.log(`Error in Service : ${err}`);
    return EMPTY;
  }
}
